---
layout: post
title: "Noobs on Ubs - Ubuntu for Beginners"
date: "2020-01-12"
draft: false
tags: ["ubuntu", "talks"]
description: "I presented a talk at Ogg Camp 2019 aimed at beginners of Linux and Ubuntu. I shared my journey - the good the bad and the ugly and what I've found along the way. This is a write up of that talk"
slug: "noobs-on-ubs"
---

# Noobs on Ubs - ubuntu for beginners

I attended {{% link name="OggCamp" href="https://oggcamp.org/" %}} in Sheffield last year and I really enjoyed it. I remember being excited to go and learn from really experienced Linux users - I thought it'd be great to learn all about Ubuntu and what it's all about but there wasn't really a talk like that.

I thought about it and decided to submit a talk for this years Ogg Camp - be the change you want to see after all! I'm surely not the only one who uses Linux but doesn't know that much about how it actually works?

I flip flopped anxiously between thinking no one would turn up because everyone is already a hard core Linux user and they know everything already or thinking people would turn up just to tell me I was wrong on some small technicality! Of course neither of things happened and I had some amazing feedback from some really great people which was lovely.

The following is the talk I gave in blog post format. I really enjoyed giving the talk and if you're interested in hearing it live, get in touch!

{{% image class="full" src="/images/noobs-on-ubs/talk-anna.jpeg" title="Me giving the talk at OggCamp" alt="Photo of me at a lectern presenting the talk in front of slide saying 'I'm the noob and this is my story'." %}}

_image coursety of {{% link name="@CarolSaysThings" href="https://twitter.com/CarolSaysThings/status/1185543269893185536" %}}_

## Why did i start using Ubuntu?

I started my career path into software development a bit later on if life - I went back to uni to study Computer Science and it was then that I made the switch from Windows to Ubuntu full time.

A few friends were using various different distributions but one friend recommended Ubuntu, what especially sold it to me was that I'd be able to get it up and running quickly and easily. I was already learning everything I could for my masters, I couldn't be going around learning extra stuff that wasn't even graded! My head would have exploded. Plus I _needed_ my laptop.

Since then I've used Ubuntu on my laptop and home PC but at work I use Windows as my company is all DotNet.

The greatest thing I got when I started on Ubuntu was the control I needed to install and run everything I needed, I found it so much easier than Windows for developing on. I also found it gave me such a better experience for telling my _why_ it wasn't working and what was going wrong so I could go about fixing it.

So I followed the {{% link name="excellent online tutorials" href="https://tutorials.ubuntu.com/" %}} (which are on point by the way) and dual partitioned with Windows.

## Was it difficult?

I got it installed so easily, all the tutorials were really helpful and the steps easy to follow. I was a little bit like "huh" either I'm a great dev or more likely it really is super easy.

As I've configured it more and more there's been some challenges along the way but luckily for me, the base install was super simple.

## When i started using linux

I was always a bit confused why it was called Ubuntu when it was Linux? And then also knowing that Ubuntu is based on Debian? So what was Ubuntu? Well I visualize it best using a Russian doll analogy!

{{% image class="full" src="/images/noobs-on-ubs/russian-dolls.png" title="Delightful Penguin Russian Dolls" alt="Russian dolls shaped like penguins going from the smallest doll up, the kernel, GNU, Debian, Ubuntu and finally the biggest doll Unity" %}}


At the very heart is the Linux kernel, the brain of the operating system. The kernel is looking after memory, running processes, handling interrupts and interfacing with the hardware.

Wrapping around the kernel is GNU/Linux, this is taking the kernel and gives all the tools to make it a functioning operating system that works. Tools such as the C compiler, bash, X Window System (allows a GUI).

Then sat on top is Debian. Debian lets us run applications and really use the computer. Debian gives us the package manager and looks after everything. Debian is also a steady stable released distro so there's stability and well tested features here.

Next, we have Ubuntu. Ubuntu wraps around Debian, provides shorter release cycles and pushes for the "newest and latest" so you don't have to wait long for new technology to be supported. Ubuntu also gives usability and support for users who don't want to run everything themselves or don't know or care how to. Also you get security out the box which is just great! Debian users know how they want to configure their setup and are quite happy to do so but when I first installed Ubuntu, I didn't have the time or energy to learn how to set it up correctly but I was concerned about the security of my set up.

And finally, I sort of thought that Debian was the OS and the desktop was Ubuntu at first. So all the colours and rounded edges was Ubuntu and then Debian was the OS. I didn't understand how other "flavours" of Ubuntu were still Ubuntu. The desktop is actually its own entity completely - one that can also be swapped!

At the time it was Unity so whenever people told me I should try something else because {{% link name="Unity" href="https://unity.com/" %}} wasn't very good - I just replied "Oh I don't do any 3D modelling so I'm cool - thanks though!". I later learned it was a very different {{% link name="Unity" href="https://unity8.io/" %}}!

## What, the desktop isn't even part of Ubuntu??

You get a desktop with Ubuntu, but you can change it or have multiple. You can also change your flavour of Ubuntu. Flavours are still Ubuntu but they're pre-configured to work how you like them. The desktop and applications are tailored for different preferences or workflows so you can find the one that suits your workflow.

<div style="margin-left: 10%">
<p>🦜 <strong><a href="https://ubuntubudgie.org/" target="_blank">Budgie</a></strong> - elegant and lightweight. Ideal for Mac users</p>
<p>🔮 <strong><a href="https://kubuntu.org/" target="_blank">Kbuntu</a></strong> - KDE and the Plasma desktop, a very popular alternative to stock Ubuntu and Gnome</p>
<p>⚡  <strong><a href="https://lubuntu.net/" target="_blank">Lubuntu</a></strong> - a fast and lightweight operating system with a clean and easy-to-use user interface</p>
<p>🕰 <strong><a href="https://ubuntu-mate.org/" target="_blank">Mate</a></strong> - a traditional desktop feel with modest hardware requirements</p>
<p>📽 <strong><a href="https://ubuntustudio.org/" target="_blank">Studio</a></strong> - a range of multimedia content creation applications straight out the box</p>
<p>🎨 <strong><a href="https://xubuntu.org/" target="_blank">Xubuntu</a></strong> - uses Xfce, a minimal, stable, light and configurable desktop environment</p>
<p>🇨🇳 <strong><a href="http://www.ubuntukylin.com/" target="_blank">Kylin</a></strong> - official Chinese version of Ubuntu</p>
</div>

So all those different flavours of Ubuntu are all Ubuntu and you get all the great Ubuntu features but it's configured and customized to work with how you may want it.

## Updating and installing stuff

I've found Ubuntu really stable and easy to use for all my regular tasks like email, web browsing, word documents etc but my struggle with installing developer applications and languages continued and I learnt a lot about Ubuntu along the way.

There's an official Ubuntu repository which is a database of all the packages that have been tested and which will work with which versions. You have a local database which holds all the current installed packages and what versions they are and if there's a newer version available to download.

Knowing how to install applications and where to put them reduces the need to `chmod 777` every single thing in the world to make something work!

What's `chmod 777`? Usually the top Stackoverflow answer to every Ubuntu permissions question. Well it gives everyone every permission on the file. It's a bit like removing the lock on your front door so whoever needs to get in can get access whenever they need to. The real solution is to only grant access to the people who need it.

### Permissions

I highly recommend reading up on permissions in Linux because it's very handy and super interesting. You can {{% link name="read more here" href="https://help.ubuntu.com/community/FilePermissions" %}} but the resource I use the most is this handy little permissions table:

<table>
    <thead>
      <tr>
        <th style="width:22%">Permission</th>
        <th style="width:19%">Binary</th>
        <th style="width:19%">Octal</th>
        <th style="width:20%">User</th>
        <th style="width:20%">Group</th>
        <th style="width:20%">Other</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>r</td>
        <td>100</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
      </tr>
      <tr>
        <td>w</td>
        <td>010</td>
        <td>2</td>
        <td>2</td>
        <td>0</td>
        <td>0</td>
      </tr>
      <tr>
        <td>x</td>
        <td>001</td>
        <td>1</td>
        <td>1</td>
        <td>1</td>
        <td>0</td>
      </tr>
      <tr>
        <td colspan="3">Total</td>
        <td>7</td>
        <td>5</td>
        <td>4</td>
      </tr>
    </tbody>
</table>

So `chmod 777 filename` is granting the User, Group and all Others the ability to read, write and execute the given file. Allowing other random programs the ability to execute may not be the best idea.

If you're installing a correctly packaged application through the recommended install method, permissions and locations are generally set when it's installing itself.

### Install stuff

Installing is all about knowing the name of the package! You can run `apt search` but I find the output sometimes confusing as I still often don't know which name to use for the package I want. Once you know the name, you simply run the following.

```bash
$ sudo apt install package-name
```

And when you need a specific version, specifying it when you install only pulls in that version if it's available.

```bash
$ sudo apt install package-name=version
```

### To update your system

To get any updates, your local packages database needs updating to see if there's any new updates available. If there are, you can upgrade them. You can see all installed packages and their versions by running `apt  list installed`.

To update:

```bash
$ sudo apt update
```
Once you've updated your package database, you can upgrade all the ones that have upgrades available by running the following:

```bash
$ sudo apt upgrade
```

Apt manages dependencies conflicts where possible and prioritizes critical or more important updates over less important ones.

### Why is there apt and apt-get?

- `apt-get`: the original CLI for managing packages. It has a lot more low level features that are not typically required for the average day to day work.
- `apt` : similar to apt-get but also provides commands from apt-cache which are useful for everyday system administration. Provides a nicer user experience, progress bar for example. _Most_ packages now a days work with apt but some need apt-get to work.

### What about if apt says no?

What about when you want to install something and apt says it's never heard of it? Or the version you've requested isn't available. This is where about 99% of my problems came from when trying to install something. I'd be working on a project with a group or started something on a uni computer and when I tried to continue with it, I'd have a version mis-match and I couldn't get the latest version I wanted on my laptop.

If you need to install something that isn't distributed as part of the main Ubuntu distribution or a newer versions isn't available, using a Personal Package Archive (PPA) is an option.

**However, tread with care!**

> PPAs are for non standard software/updates. They are generally used by people who want the latest and greatest. **If you are going extra lengths to get this kind of software, then you are expected to know what you are doing. The selection in the Software Centre is ample for most human beings.**

{{% link name="dire warning from the ask ubuntu stack exchange" href="https://askubuntu.com/questions/4983/what-are-ppas-and-how-do-i-use-them" %}}

> If they aren't already available in the Ubuntu repositories, that means they have not yet undergone sufficient testing to be imported into the main archive

{{% link name="explanation from the ubuntu community" href="https://help.ubuntu.com/community/PPA" %}}

The PPA key is added to your sources list which means you can use apt install and update to manage the package. Using a PPA is a great way to use a package that's not from the Ubuntu repository however be aware that installing ropey PPAs can cause system problems. When you install or upgrade a package from the Ubuntu repositories or a PPA, you're giving it root access to your system so installing from verified and trusted places is really important.

Plus is might just plain old break everything - nothing malicious intended just getting stuff to work together is _hard_.

## Ain’t no party - like a linux hierarchy

When I learned the basics of the file hierarchy, things really started to make sense a lot better for me.
Here's a quick intro to the highlights of the Linux file hierachy following the Debian standard.

Once I understand the filesystem better, gone were the days when I would download a zip of nodeJS and extract it in every file I could find hoping that one would work.

### The / Root Filesystem

Everything in Linux is a file.

{{% fileTree %}}
* /
{{% /fileTree %}}

The root filesystem. Everything in the root is for booting, restoring, recovering and repairing the system.

{{% fileTree %}}
* bin
{{% /fileTree %}}

Bin is for Binaries (or Executables). The root level bin is for binaries used for minimal system working and can be ran by any user.

Examples include the binaries for the commands pwd, mkdir, cat and systemctl.

{{% fileTree %}}
* boot
{{% /fileTree %}}

Boot is for static files for the bootloader.

{{% fileTree %}}
* dev
{{% /fileTree %}}

Dev is for devices. This is where the physical devices are mounted. The device files are interfaces for the systems device drivers. This allows applications to interact with the device driver exactly the same as other files - using the standard input and output system calls.

{{% fileTree %}}
* etc
{{% /fileTree %}}

Mostly pronounced et-cee like Betsy or spelled out eee-tee-cee but who cares. From the definition of et cetera, "additional odds and ends; extras".

Etc is for configuration files. These are system wide configurations used to control the operations of a program.

Example, information for users: the groups, users, names, IDs etc and the shadow file (the encrypted passwords of all the users on the system) also your hosts file.

These files are typically used by the system administrator.

{{% fileTree %}}
* home
{{% /fileTree %}}

User home directory. For all your stuff like documents, pictures, projects etc.

{{% fileTree %}}
* lib
{{% /fileTree %}}

System shared libraries for booting and running commands in the root filesystem.

Example, cryptsetup library.

{{% fileTree %}}
* media
{{% /fileTree %}}

This is where media gets mounted to. If you plug in a USB for example.

{{% fileTree %}}
* opt
{{% /fileTree %}}

Add-on application software packages, all data required to support a package on a system must be present within it's directory.

### The /usr Hierarchy

{{% fileTree %}}
* usr
{{% /fileTree %}}

Usr is not for *user*. I repeat, _not_ user!

I always thought it meant my user applications should go there. And some of it does, hence my confusion being justified. Apparently it was once someones user directory when the `/bin` file got too big for one floppy disk and everything that needed to be there ready for the second disk went under `/usr`

Most other system files are duplicated here under `/usr/`.

It's for all system-wide, read-only files installed by (or provided by) the OS.

If you have multiple machines, they can all mount the same `/usr` directory and share the applications. This makes maintaining and updating pretty straight forward.

{{% fileTree %}}
* usr
    * bin
{{% /fileTree %}}

This is the primary directory for executables that are not needed for booting or repairing the system.

Example would be Firefox or Libre Office and commands like less, pkill and which.

{{% fileTree %}}
* usr
    * lib
{{% /fileTree %}}

Libraries for programming and packages installed that aren't system dependant.

{{% fileTree %}}
* usr
    * local
{{% /fileTree %}}

The `local` directory is for installing software locally by the administrator (probably you if it's your machine) and again the file structure is duplicated. When it's saved here it's not going to be overwritten when the system software is updated.

{{% fileTree %}}
* usr
    * local
        * bin
{{% /fileTree %}}

So in `/usr/local/bin`, there's the binaries for programs locally. As in programs _not_ managed by the distribution package manager.

For example, Dropbox as it isn't included in the official Ubuntu repositries.

{{% fileTree %}}
* usr
    * share
{{% /fileTree %}}

Shares is for architecture-independent data, mostly man pages and locale, languages information. Also ASCII character tables, printer definitions are stored here.

### The Var Hierarchy

{{% fileTree %}}
* var
{{% /fileTree %}}

Var is for Variable Data Files. Contents include temp files like log files.

`/var` is in the root file so that the `/usr` can be mounted as read only. Anything that needs to write out is written out to `/var` now.

{{% fileTree %}}
* var
    * cache
{{% /fileTree %}}

Cached data from applications to save IO speed. These files can be deleted at any time by the user or removed when running low on disk space therefore all applications must be able to recover if they are removed.

{{% fileTree %}}
* var
    * games
{{% /fileTree %}}

Any variable data relating to games in `/usr` will be saved here.

If you want more information, type `man hier` in the terminal or check out the {{% link name="Debian standards" href="https://www.debian.org/doc/packaging-manuals/fhs/fhs-3.0.html" %}} for the file hierarchy.


## Snaps got your back

One way to solve the difficult problem of version dependencies and to try and reduce the time to get an updated package is Snaps.

> A .snap package for the Ubuntu Core system contains all its dependencies. This has a couple of advantages over traditional deb or rpm based dependency handling, the most important being that a developer can always be assured that there are no regressions triggered by changes to the system underneath their app.

{{% link name="from the snap site" href="https://snapcraft.io/" %}}

Snaps work independently of the Ubuntu release cycle so for packages like Spotify or Krita, the user isn't waiting so long for new features meaning you're using the latest a lot quicker. Of course this comes with caveats - you're no longer in charge of your own updates and you're trusting the snaps in your system to push whenever they like.

When snaps were introduced a lot of Linux users were uncomfortable using them and that's totally fine - the best thing about Ubuntu and Linux distro's in you're in charge! If snaps don't work for you - you don't have to use them. Simples.

## Closing remarks

I found moving from Windows to Ubuntu pain free and picked up the new OS pretty easily at a time when I was heavily reliant on my computer and had no time to learn something new. I completely recommend Ubuntu to everyone from elderly relatives to the kids at Code Club.

It's not always been plain sailing - my biggest "Oh no" moment with Ubuntu was when I realized there's no `Ctrl` + `Alt` + `Delete` (Windows users will know what I mean!). At first I didn't know what to do when things were crashing (things always crash on Windows so don't give me the old well if it's crashing why use it speech) but I soon learnt what to do.

### So if ever in doubt

- Try turning it off and on again
- Check what you’re running with `which <program name>`
- Check the version you’re running, usually `--version`
- Ask someone, there’s a huge community of people who have probably had this problem before

And if all else fails,

- Re-install! Keep everything backed up and you'll never have any problems

## Resources
- {{% link name="Ubuntu's excellent online tutorials" href="https://tutorials.ubuntu.com/" %}}  
- {{% link name="Ubuntu File Permissions explained" href="https://help.ubuntu.com/community/FilePermissions" %}}
- {{% link name="Ubuntu PPAs explained" href="https://help.ubuntu.com/community/PPA" %}}
- {{% link name="More information about Snaps" href="https://snapcraft.io/blog/8-ways-snaps-are-different" %}}
- {{% link name="The Debian Filesystem Hierarchy Standard" href="https://www.debian.org/doc/packaging-manuals/fhs/fhs-3.0.html" %}}
- {{% link name="apt vs apt-get - The Difference" href="https://itsfoss.com/apt-vs-apt-get-difference/" %}}
- {{% link name="Link to my slides for the original talk" href="https://talks.annadodson.co.uk/noobs-on-ubs/#/" %}}
- {{% link name="Link to the video of the talk" href="https://youtu.be/ardp-z4-ROs?t=13523" %}}

### With thanks

Thank you {{% link name="Super Tiny Icons" href="https://github.com/edent/SuperTinyIcons" %}} for your fantastic super tiny svg logos. Thank you to everyone who attended the talk - I hope you enjoyed it and special thanks to the kind people who left feedback.

A massive thank you to {{% link name="Jamie Tanna" href="https://www.jvt.me" %}} for proof reading this post, listening to me rehearse my talk and always inspiring me to keep learning.

🧡
