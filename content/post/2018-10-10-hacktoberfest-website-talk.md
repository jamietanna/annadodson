---
title: "We Built This For Hacktoberfest"
date: "2018-10-10"
tags: ["community", "hacktoberfest"]
draft: true
description: "Building a community website for hacktoberfest - encouraging and helping people contribute to open source"
slug: "hacktoberfest-website-talk"
---

<h3>Hacking for Hacktoberfest</h3>

This year I've done a lot of Hacktoberfest promoting to try to encourage and help beginners and non-coders into contributing to free and open source software. I did some shout outs at Tech Nottingham and Women in Tech meetups and also spoke and helped out at a Uni of Nottingham HackSoc event.

<!-- more -->

To try and explain why I think it's important to contribute to open source, here's the slides of the talk I gave at HackSoc.

Inspired by Care (TODO name and twitter link) and the October Women in Tech meetup, I gave drawing my slides a go.

Ok, So why do we care about Free and Open Source Software?

Here's Tracey, she's a farmer and she bought a big fancy tractor for her farm. If her tractor has a problem ,she's not allowed to repair it. Even though it's _her_ tractor! So if it breaks down mid critical time of the year, she is forced to wait for an engineer to come out to her and sort it out. She's not allowed to try and sort out the problem herself.

// TODO tractor slide here
()[]

Another example that's maybe a little closer to home. You buy a lovely new laptop that's super powerful and shiny. You really like it and use it all the time. You're using it so much that your battery is running down. So you decide to turn off the fancy light on the back of the laptop lid to save power. Oh no, wait, you can't. Even though it's _your_ laptop. Or how about the icon tray is really annoying and you want to move it? You can't. Even though, it's your laptop.

// TODO add pic of mac laptop

Or how about you buy a different kind of laptop. And you decide you want to change the start button. It's annoying where it is and you want to customize it. Maybe you're super into pandas and you want a picture of a panda instead of the start button. Well you can't. Even though, it's _your laptop_. 

// TODO add pic of windows laptop

But don't worry - if you want a panda and to be able to customize your laptop how you like, there's linux! Woo! So you can update, change and make amends to your hearts content. And if you do make changes and want to make that script available to other people, you can upload the code on GitLab or GitHub or any other site you like. As long as you license your code correctly, you can give your panda solution out to anyone who wants it! And then if someone finds it and loves it so much that they want to update it to include other animals (maybe a zebra that is also black and white?) your license should allow them to do that. In fact your license can let them take your code, use it and if they want to modify it, they can share those changes back with you. Pretty cool.

Jamie (TODO link) spoke about the different licenses there are and how you can go about choosing the best option for your project. His slides are up here too.
