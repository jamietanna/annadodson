---
layout: post
title: "Ubuntu - Create a new Partition and Mount it on Start up"
date: "2019-09-08"
tags: ["ubuntu", "blogumentation"]
description: "Making a new device a partition and mounting it on start up"
slug: "mounting-on-boot"
---

I have a separate SSD hard drive in my computer so I decided to use it to save games and other similar content on. Because it's mainly for games, I decided I don't need to encrypt it.

I followed these steps to create the new partition and set it to be mounted at boot time into a `games` directory.

## Step One - create and format the partition

🚨 This step is for _new_ devices or if you want to change what the drive is for.
If you already have a formatted drive with data on it, you do not need to do this step.

You can probably do this in gparted if you already have that installed but I did it with {{% link href="https://kde.org/applications/system/org.kde.partitionmanager" name="KDE's Partition Manager" %}} because I was using that for managing my other partitions.

Select the device (hard drive) you want to use. Mine was called `sda` because it's the first disk, my primary disk is the second one so it's called `sdb`. By clicking on the top corner, it gives you information about each drive so you can work out which one is which.

Next, make a new partition table of type GPT (GUID Partition Table).

Then click 'New' to make a new partition. Configure it as `ext4`. You can set it to whatever size you want to. My partition was then called `sda1` as it's the first partition on the drive.

## Step Two - make sure it has a mount point

If it hasn't already been mounted somewhere, you'll need to create a mount point. This is the directory you want the new drive to be accessible from. I made a new directory at the root:

{{% fileTree %}}
* /
  * games
{{% /fileTree %}}

Once that's created, you can set the drive to mount automatically on boot.

## Step Three - edit your fstab file

> The configuration file /etc/fstab contains the necessary information to automate the process of mounting partitions. In a nutshell, mounting is the process where a raw (physical) partition is prepared for access and assigned a location on the file system tree (or mount point). 

<p style="text-align:right;"><a target="_blank" href="https://help.ubuntu.com/community/Fstab"><i>FSTab from ubuntu wiki</i></a></p>

You can view your fstab file to see what is currently mounted at start up:

```bash
$ cat /etc/fstab
```

To get the UUID of your new device, run:

```bash
$ blkid
```

Find your new device and make a note of the UUID. You need to add your new device to the end of the fstab file.

🚨 Make a copy of your fstab file so you can easily roll back if you have any problems.

Here's an example of the settings I chose:

```bash
UUID=b919kaca-5ec4-4be7-8855-a2b89f38a116 /games ext4 defaults 0 2
```

This is set for my device sda UUID, with mount point `/games` which is a file system type `ext4`, with all default options, 0 to disable the back-up of the partition and 2 for error checking at boot time but with a priority lower than root.

You'll need to open the file as an administrator to be able to edit it.

To test it's working ok, unmount the device if it's already mounted:

```bash
$ umount <mount-point>
```

Then to mount everything in your fstab file, run:

```bash
$ mount -a
```

Your new device should be mounted :tada:

