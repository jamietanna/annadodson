---
title: "Becoming an Organiser of Women in Tech Notts"
date: "2019-07-08"
draft: false
tags: ["community", "wit"]
description: "For the last few years, the Nottingham Tech community has been a big part of my life and this year I became an organiser for Women in Tech"
slug: "becoming-an-organiser"
---

# The Community

Since becoming a developer and attending meetups in Nottingham, my confidence has grown, I've learned loads about loads of different interesting subjects and I've met some of my closest friends.

I love all the meetups I attend, all the ones I'm too busy to get to and the ones I haven't been to yet but I have a special place in my heart for WiT.

I love the Women in Tech meetups, it was a revelation to meet people like me. We have shared interests, goals and we can talk and discuss topics we're interested in.

After being a regular attendee for ~2 years and helping whenever needed I've seen how much WiT has grown and got busier. Emma and Helen asked me if I'd like a WiT branded hoodie and _of course_ I did (badly!).

Since then I've been on the team, planning activities, resources, speakers, prizes and whatever else crops up!

Being able to help a community which has helped me is an amazing feeling. I remember when I first started attending, I would walk in and not know anyone and feel really nervous. I never knew what to do or say, I try and remember that when we're organizing, how we can make the event welcoming to new and old members.

I know from my own experiences that being a minority can be challenging and being involved in WiT helps support a cause I really care about. I'm also aware that I'm very privileged as a white cis gendered heterosexual person and there's lots of challenges I don't face. I hope I can learn from people who do face daily struggles from being a minority, I really want WiT to be an open and understanding community where people can come and have fun and meet like-minded people free from any prejudices.

{{% image src="/images/wit-banner.jpeg" alt="Women in Tech banner" %}}

# What does being a volunteer mean?

Obviously the best thing about being an organiser is that sweet sweet hoody! :stuck_out_tongue_winking_eye:

Swag aside, helping a community that means so much to me is pretty awesome. I get to be creative and speak to loads of different people. We like to plan activities that we think would be good fun and that will bring people together. We love thinking of prizes that tie in with the talk and spark conversation and interest amongst attendees. I especially love attendees recommending topics for talks, books as prizes or activities to do. If you have any ideas, please share them! This event is the attendees event, not the organisers.

Organising and volunteering can be time consuming at times but that's why being on a great team really helps - when one person's busy, there's others to support and an awesome community to step in and help out too. 

Thank you to 
{{% link href="https://twitter.com/MrsEmma" name="Emma" %}},
{{% link href="https://twitter.com/MrAndrew" name="Andrew" %}},
{{% link href="https://twitter.com/CarolSaysThings" name="Carol" %}},
{{% link href="https://twitter.com/LittleHelli" name="Helen" %}} and
{{% link href="https://twitter.com/short_louise" name="Louise" %}}
for being an awesome team 🧡
