---
title: "Git Reference Guide - part three"
date: "2019-01-03"
draft: true
tags: ["git"]
intro: "Part three is a list of the commands _I think_ are the best, the most handy and the ones that are super useful but I don't use that often so I need to write them down somewhere."
slug: "getting-git-part-three"
---

## My Top Git Commands

These are the most common commands I run and ones I use not often enough to remember off the top of my head but enough to note them down to save googling each time.

```
$ git add -p <file-name>
```
Patch, can can add just the section of the file you want to. If you've changed a few things or left some debugging in the file, add the bits you want and then you can checkout (discard) the remaining changes to the files. Whilst you're running the patch you are given options `Stage this hunk [... ` type `?` to see what each option does.

Note, this is short hand for running `git add -i` which is an interactive add. You can perform a patch add or diffs using interactive add.

```
$ git commit -v
```
A verbose commit, open vim (or your editor of choice) to write your commit message and see all your changes to the files. Very handy for remembering your changes and explaining them in the commit message.

```
$ git branch -m new-name
```
To rename the branch that you're currently on. If you're on a different branch, run:
```
$ git branch -m old-name new-name
```

