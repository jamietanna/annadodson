---
title: "Networking for Muggles"
date: "2017-09-18"
description: "Talk at Linney about the web and networking"
tags: ["talks"]
slug: "how-to-build-the-web-talk"
---

<h3>How to get a website. Explaining networking to non-technical stakeholders</h3>

How to explain web development to non technical work people? There's many different stakeholders involved in building a website in a web agency, how to explain how it's built and put together if they have no knowledge of web building?


As a team we were given the topic of explaining 'The Modern Web' to print designers, project managers and account directors. We wanted to share knowledge to explain basic problems and time drains we often face. What can speed us up and what can slow us down and therefore cost the business? 

I decided Show and Tell always works well, we took some of our testing devices and set them up at the front of the room on the same website. The devices ranged from Android and iPhones, tablets, laptops and a desktop monitor. One website to rule them all. Whats the difference in speed and cost on these different devices? Viewing on mobile on the go vs our client sat at his desk looking at the site on his monitor.

I wanted to explain basic networking, I often get asked how a customer can get a domain or how we can use their domain to point to our website. Or frustrations when there's a caching problem. I explained the age old interview question about what happens when you type an address into the address bar.


<div style="margin: 30px 0;">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQtokccEZsFr-7AvThqIu3AvQgnH7_haZw786Y4CadRdMNbTfYtcdSLIOi2GCF_8JhnUpXq7GgDKirq/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</div>

I enjoyed this talk, it was business relevant and aimed at improving our understanding of each other, what we can offer customers and what we don't need to offer customers. 
