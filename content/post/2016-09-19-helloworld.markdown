---
layout: post
title:  "Hello World"
date:   "2016-09-20"
categories: example
description : "Welcome to my new website! Find out what I've been up to. Spoilers, I'm now looking for work."
slug: "helloworld"
---

<h3>Welcome to my new website!</h3>

I've just finished doing my Masters in Computer Science at the University of Nottingham. I handed in my summer project last week and I'm now doing freelance web development and looking for a new career opportunity.

<!-- more -->

I've spent the last three months building a web application for remote pair programming as a learning tool. I wanted to build a tool which would help encourage girls into CS and I spent a lot of time researching learning behaviours through team work and collaboration. I built the app in HTML, JavaScript/JQuery, NodeJS and socket.IO and pushed it live on a server to conduct user studies. You can find more details <a href="/projects#project">here</a>.

Right now I'm building websites, attending as many Hackathons as I can, building up my java for Android development and completing Hackerrank challenges and learning bash along the way.

I'm looking for my next career move and more opportunities to volunteer within the tech community, if you want to hear more about me or have an opportunity you'd like me to get involved with, <a href="mailto:{{< param email >}}?Subject=I%20Want%20To%20Hear%20More">get in touch</a>.

