---
layout: post
title: "Hacktoberfest"
date: "2016-11-01"
tags: ["hacktoberfest", "open-source"]
description: "2016 Hacktoberfest"
slug: "hacktoberfest"
---

<h3>October is for Beer and Hacktoberfest</h3>

Super proud to have taken part in Hacktoberfest this year! It was hard to find stuff to contribute too, loads of projects I wanted to contribute to but not many I felt I _could_ do.
Aim for next year, have loads more contributions under my belt!

<!-- more -->

---

My four contributions were (in order of preference):

1. <a href="https://github.com/HackSocNotts/wit-deprecated" target="_blank">The Inspire Women in Tech University of Nottingham Conference website</a> - a subject close to my heart. Hacktoberfest gave me the confidence to ask to contribute to the website. The HackSoc committee members pointed out the <a href="https://github.com/HackSocNotts/wit-deprecated#todo" target="_blank">TODOs</a> on the README and said removing the PHP dependency would be a great help. Making the site static would make it easier to update and easier for anyone to contribute to as running the site locally would be a lot simpler and quicker. A lower barrier of entry is a great way to encourage more people to contribute.

   I went through the site and replaced any PHP functionality with tasks in the grunt file so the site could be built into static HTML. The HackSoc team gave me some great pointers for committing and merging on an Open Source project and I successfully sent my first pull request. I was especially happy that it was for such an amazing project that I had helped organise and <a href="https://twitter.com/kerpowla" target="_blank">Paula</a> and the team had built the site a year before exactly for the purpose of encouraging more women into tech. Warm fuzzy feelings all round!

   I did learn that as with Open Source projects, changes in leadership can affect the direction projects go in and unfortunately the PR wasn't merged in but I'm hoping it will get merged ready for the next conference.

2. <a href="https://github.com/AnnaDodson/HackSocNotts.github.io" target="_blank">HackSoc University of Nottinghams Hack Society Website</a> - I volunteered to help out at a Git Hacktoberfest workshop that the society was running to encourage people to use Git and to contribute to Open Source projects. The aim of the workshop was to get everyone set up with a GitHub account and to make a commit to the HackSoc website on a blog post about Hacktoberfest. The workshop was great fun and really nice to chat to all the new society members. As usual the session ran over and as it was very popular, it was hard to get round everyone for the worhsop part of the session so I didn't get as many commits onto the page as I would have liked but everyone left with a Github account and an understanding of ssh keys and how to clone a project which I was really happy with. Especially remembering how long it took me to figure out ssh keys!

   This PR was just a small one, just my details - no functionality or issues closed off but I really enjoyed the teaching and helping others that took place behind the scenes of this PR.

3. <a href="https://github.com/AnnaDodson/instantsearch-workshop" target="_blank">Algolia Workshop Example Project</a> - An example project to get started with Algolia's API, demo'd at Hackference 2016. At the hack weekend part of Hackference, I attended the Getting Started workshop as I was hacking with their API. During the workshop, Josh Dzielak (Algolia developer) noticed an error in one of the files so I corrected it and sent a PR.

   A simple easy PR made at the time the error was noticed. I liked this PR because it was so easy and helped everyone getting started with the template repository.

4. <a href="https://github.com/AnnaDodson/cb-v2-scratch" target="_blank">The Code Buddies project repo</a> - the first project I found through the Hacktoberfest tagged repos for beginners. The project is aimed at helping build a community and teach other people which really appealed to me and felt like a really great welcoming project community to contribute to. I looked through a lot of issues and attempted to get to grips with the project but in the end, the PR I made was a license for the repo of the project which was missing a license - a potential problem for an Open Source project with so many active users and contributors.

   The feedback I got from the PR was great and was merged in after the maintainers had discussed the license further.

---

I spent a lot of time trawling projects and reading open issues tagged with Hacktoberfest. It was very daunting but all my PRs were overall a really positive experience and even the one that wasn't merged was a huge lesson to me. I'm looking forward to next year and I now have more community projects I'm contributing too which is actually pretty cool.

Cheers GitHub and Digital Ocean for supporting and hosting such an awesome event.

{{% image class="full" src="/images/hacktoberfest.jpg" title="Hacktoberfest t-shirt" %}}
