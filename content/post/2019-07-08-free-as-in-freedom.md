---
title: "Free as in Freedom, not as in Pizza. Free and Open Source - a Talk at WiT"
date: "2019-07-08"
draft: true
tags: ["community", "wit", "talks", "open-source"]
description: "I gave a talk at Women in Tech Notts meetup on Free and Open Source Software"
slug: "free-as-in-freedom"
---

# Free and Open Source Software - what is it and why should we care?

About 6 months ago I gladly signed up to give a talk on "something" at WiT in August. So I obviously left it until two weeks before to start writing it.

I wasn't too sure what to talk on and Emma suggested I talk about Open Source as I spoke briefly about it when I was running a Hacktoberfest site for the community to get involved in.
