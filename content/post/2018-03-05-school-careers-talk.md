---
title: "School STEM Careers Talk"
date: "2018-03-05"
tags: ["talks"]
description: "I spoke at Notts Girls High School about working as a software engineer"
slug: "school-careers-talk"
---

<h3>Nottingham Girls High School Careers Event</h3>

I spoke at the NGHS Careers in STEM evening about my role as a Software Engineer and what I actually do every day. I also tried to answer some common stereotypes and myths I often come across. The evening was aimed at encouraging the students into STEM careers by showcasing some of the different careers out there.

<!-- more -->

The school often hosts different career evenings, last year myself and some of the other members of <a href="https://www.meetup.com/Women-In-Tech-Nottingham/">Women in Tech Notts</a> had a stand, this year it was an evening of short talks by different women talking about their career journeys and roles. These  within Engineering, Stem Cell Research, Medicinal Chemistry, Radiotherapy and Medicine.

I want good career advice to be available to all school students and evenings like this are great to showcase the different kind of industries and roles that are out there. When I was at school, I'm not even sure my job even existed like it does today so it's also really important to tell students their paths aren't set in stone by their GCSE choices. Which is something I definitely thought was true when I was doing my GCSEs.

<div style="margin: 30px 0;">
  <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://docs.google.com/presentation/d/e/2PACX-1vRxTnl8lHFhAh6GcACr8pOZtqty-pybrHUxVlc2aN2seMtJLv6QdTS9eauD1mKKq5jaKVzsKHBlXpkR/embed?start=false&loop=false&delayms=3000" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
  </div>
</div>

I started by talking about how I got into a tech role, how I'd always liked the theatre and working backstage which is what eventually led me to programming and Computer Science.

Next I did a short "day in the life". I'm always curious about other people jobs, what the they _even do_ all day!? I wanted to explain that being a software engineer is _not_ a solo career where you never speak to anyone which I think is a common misconception.

I then ran through some FAQs and explained how some myths are not myths - some programmers are massive gamers and some are not. This is true in life! Some people do stuff, others don't. It does not affect anyone's ability to be interested in anything.

I took the advice of a girl who is currently doing a work placement at my company; she told me that when she started her CS undergrad, she had no idea about the many different jobs that can be encompassed within "programming" or "software" so I spoke a little about roles in AI, machine learning, hardware, graphic, gaming.

After the talks all the speakers hung around for any questions from students or parents. Most left straight away but I spoke to a few students which was awesome helping to answer some of their questions 

Hopefully the evening helped some of the students and I definitely really enjoyed it. I'm hoping to do more outreach in schools and I'm looking into speaking at some of the less affluent schools in and around Nottingham - not just aimed at girls.

