---
layout: post
title:  "Spirit of the Hack"
date:   "2016-10-29"
tags: ["hackathons"]
slug: "spiritofthehack"
---

<h3>Hackference Hackathon</h3>

Last weekend I was at <a href="https://2016.hackference.co.uk/hackathon.html" target="blank">Hackference!</a> A weekend hack in Birmingham for students and non students sponsored by Algolia, Microsoft, Pusher, Pebble and loads more. 

<!-- more --> 

I was in a team with <a href="https://jvt.me/" target="blank">Jamie Tanna</a> and we hacked on CalGolia, a calendar application to find a date you and your friends are free. Inspiration: I'm one of 7 bridesmaids and trying to find a date when we're all free is ridiculous. All the WhatsApp notifications!!! 

We used <a href="https://www.algolia.com/" target="blank">Algolia's super fast search</a> because we went to their workshop and it seemed a pretty cool API and a good way to search for the free time. To get the busy times, we took a calendar ics file and parsed it in Python using the <a href="http://icalendar.readthedocs.io/en/latest/" target="blank">iCalendar</a> package which got all the events for each participants calendar into json objects for Algolia. We used <a href="http://flask.pocoo.org/" target="blank">flask</a> (a microweb framework) for the frontend and parsed the json calendar objects from algolia and presented it to the frontend.

<a href="https://twitter.com/dzello" target="blank">Josh Dzielak</a> from Algolia was really friendly and when we ran into some trouble with widgets he helped us out and even gave us a couple of free t-shirts at the end! Really good hack swag.

We didn't quite get the search function working fully - too much fun at dinner time. We had hoped to be able to type, for example, "Friday" and be given a list of the next few Fridays when all participants were free. But hopefully we can continue working on it because I really need this in my life.

This was the most successful hack project I've ever done so I was super happy to have contributed a large part of the functionality and that we got most of it working. A long way from the first few hacks I went to where I had no clue where to even start! I think <a href="https://twitter.com/ProactivePaul" target="blank">Proactive Paul</a> could tell how happy we were with our hack and awarded us one of the two Spirit of the Hack awards which was some Star Wars Lego!!!

Proactive Paul is my new hero and he gave one of the most inspirational speeches I've ever heard at the welcome speeches. <a href="https://twitter.com/hackferencebrum/status/789785440157065216" target="blank">#paulForPresident</a>.

The hack was great fun, a really nice atmosphere and the food was great! Massive thanks to all the team who organised, helped and ran the weekend, especially <a href="https://twitter.com/ukmadlz" target="blank">Mike</a>. My top hacks were: 

- Silver Haze - a prom dress with sewn in LEDs which could be controlled through your pebble watch
- Freida - Chrome plugin using Microsoft face recognition to click on a face when you're watching a film to open the actors wikipedia page
- Elsmore - a useful tool to fetch websits and save them locally from the command line

Plus loads more great ideas and presentations.

Despite the rumors it won't run next year, I really hope it does because I would love to go again.

CalGolia is on GitLab if you're interested, <a href="https://gitlab.com/jamietanna/calgolia" target="blank">take a look!</a>
