---
date: "2017-06-26"
title: "Anna Dodson"
---

I'm a .NET developer at {{% link href="https://imosphere.co.uk/" name="Imosphere" %}}, working on healthcare applications that help improve patient care.

In my spare time I love getting involved in the {{% link href="https://www.technottingham.com/" name="Tech Nottingham" %}} community, I'm on the organising team for {{% link href="https://www.meetup.com/Women-In-Tech-Nottingham/" name="Women in Tech" %}} and I'm currently volunteering at a local {{% link href="https://codeclub.org/en/" name="Code Club" %}} when I have a spare weekend.

I live with my partner {{% link href="https://jvt.me" name="Jamie Tanna" %}} and our little horror of a cat, {{% link href="https://twitter.com/anna_hax/status/1030573279831105536" name="Morphs" %}}.

I like Linux - primarily {{% link href="https://www.ubuntu.com/" name="Ubuntu" %}}, Free and Open Source Software, messing around with my {{% link href="https://gitlab.com/AnnaDodson/configs" name="configs" %}} and automating stuff, especially our home!

My blog is my place for documenting stuff I've learned and talking about things I care about.

{{% link href="mailto:hi@annadodson.co.uk" icon="fa-envelope-o" name="Get in touch if you want to say hi" %}}!
