Personal Website
=====

Recently moved from Jekyll to Hugo.

New template taken from: https://github.com/zwbetz-gh/cupper-hugo-theme.

An accessibility-friendly Hugo theme, ported from the original Cupper project.

License: MIT License.

## To run and build

To build and run locally, first install Hugo extended
** Note the extended is very important for sass support

To run locally:
```
hugo serve
```

To build
```
hugo
```
